<?php

use PHPUnit\Framework\TestCase;

require "vendor/autoload.php";

/**
*
* @requires PHP 5.6
* @author Eduardo Azevedo eduh.azvdo@gmail.com
*
* @coversDefaultClass /lib/theMovieDatabaseHelper
*/
class theMovieDatabaseHelperTest extends TestCase
{
	private $tmdb;

	public function setUp ():void
	{
		$this->tmdb = new \lib\theMovieDatabaseHelper('4ec327e462149c3710d63be84b81cf4f');
	}

	public function testMovie ()
	{
		$raw = array(
			'id' => 585, 'title' => "Monsters, Inc.", 'tagline' => "We Scare Because We Care.",
			'overview' => "James Sullivan and Mike Wazowski are monsters, they earn their living scaring children and are the best in the business... even though they're more afraid of the children than they are of them. When a child accidentally enters their world, James and Mike suddenly find that kids are not to be afraid of and they uncover a conspiracy that could threaten all children across the world.",
			'release' => "Nov 1, 2001", 'genres' => "Animation, Comedy, Family",
			'poster' => "https://image.tmdb.org/t/p/w500/sgheSKxZkttIe8ONsf2sWXPgip3.jpg"
		);
		$result = $this->tmdb->movie('585');
		$this->assertEquals($raw, $result, "Single movie");
	}
}

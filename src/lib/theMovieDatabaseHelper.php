<?php

namespace lib;

/**
 * Class to handle TheMovieDB's API requests
 */
class theMovieDatabaseHelper
{
	private $key;
	private$configuration;

	public $image_base_url;
	public $genres;

	/**
	 */
	public function __construct (string $key)
	{
		$this->key = $key;

		$_raw_conf = file_get_contents('https://api.themoviedb.org/3/configuration?api_key='.$key);
		$this->configuration = json_decode($_raw_conf, true);
		$this->image_base_url = $this->configuration['images']['secure_base_url'];

		$_raw_genreslist = file_get_contents('https://api.themoviedb.org/3/genre/movie/list?api_key='.$key.'&language=en-US');
		$this->genres = json_decode($_raw_genreslist, true)['genres'];
	}

	/**
	 */
	public function trending ()
	{
		$list = array();
		$raw_data = file_get_contents('https://api.themoviedb.org/3/trending/movie/week?api_key='.$this->key);
		$decoded_data = json_decode($raw_data, true);

		foreach ($decoded_data['results'] as $data) {
			$list[] = array(
				'id' => $data['id'],
				'name' => $data['title'],
				'overview' => $data['overview'],
				'release' => date('M j, Y', strtotime($data['release_date'])),
				'genre' => $this->_genres($data['genre_ids']),
				'poster' => $this->_poster($data['poster_path'])
			);
		}

		usort($list, function ($a, $b){ // order by name
			return strcmp($a['name'], $b['name']);
		});

		return $list;
	}

	/**
	 */
	public function searchByName (string $arg)
	{
		$list = array();
		$_urlquery = 'https://api.themoviedb.org/3/search/movie?api_key='.$this->key.'&language=en-US&query='.urlencode($arg);
		$raw_data = file_get_contents($_urlquery);
		$decoded_data = json_decode($raw_data, true);

		foreach ($decoded_data['results'] as $data) {
			$list[] = array(
				'id' => $data['id'],
				'name' => $data['title'],
				'overview' => $data['overview'],
				'release' => date('M j, Y', strtotime($data['release_date'])),
				'genre' => $this->_genres($data['genre_ids']),
				'poster' => $this->_poster($data['poster_path'])
			);
		}

		return $list;
	}

	/**
	 */
	public function searchByGenre (string $arg)
	{
		$list = array();
		$_urlquery = 'https://api.themoviedb.org/3/discover/movie?api_key='.$this->key.'&language=en-US&with_genres='.$arg;
		$raw_data = file_get_contents($_urlquery);
		$decoded_data = json_decode($raw_data, true);

		foreach ($decoded_data['results'] as $data) {
			$list[] = array(
				'id' => $data['id'],
				'name' => $data['title'],
				'overview' => $data['overview'],
				'release' => date('M j, Y', strtotime($data['release_date'])),
				'genre' => $this->_genres($data['genre_ids']),
				'poster' => $this->_poster($data['poster_path'])
			);
		}

		return $list;
	}

	/**
	 */
	public function movie ($id)
	{
		$raw_data = file_get_contents('https://api.themoviedb.org/3/movie/'.$id.'?api_key='.$this->key.'&language=en-US');
		$data = json_decode($raw_data, true);

		$movie = array(
			'id' => $id,
			'title' => $data['original_title'],
			'tagline' => $data['tagline'],
			'overview' => $data['overview'],
			'release' => date('M j, Y', strtotime($data['release_date'])),
			'genres' => join(', ', array_map(function($m){ return $m['name']; }, $data['genres'])),
			'poster' => $this->_poster($data['poster_path'])
		);

		return $movie;
	}



	private function _genres (array $ids)
	{
		$_genres = array_map(function ($g) use($ids) {
			if (in_array($g['id'], $ids))
				return $g['name'];
			return null;
		}, $this->genres);
		return join(', ', array_filter($_genres));
	}

	private function _poster ($path)
	{
		return $this->image_base_url.'w500'.$path;
	}
}

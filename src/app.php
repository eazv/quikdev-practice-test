<?php
// Instantiate the app with defined settings
$app = new \Slim\App(['settings' => array(
		'displayErrorDetails' => true, // set to false in production
		'addContentLengthHeader' => false, // Allow the web server to send the content-length header
	)]);

/* dependencies configuration */
$container = $app->getContainer();

/* Views handler */
$container['view'] = function ($c) {
	return new \SimpleView\SimpleView([
		'folder'   => __LOCAL__.'src/views',
		'base_url' => '/',
		'assets'   => [
			'js'        => ['/js/'],
			'css'       => ['/css/'],
			'vendor'    => '/assets/',
			'image'     => '/img/',
		],
	]);
};


// The Movie DB API helper provider
$container['tmdb'] = function ($c) {
	return new \lib\theMovieDatabaseHelper('4ec327e462149c3710d63be84b81cf4f');
};


/* Not found page handler */
$container['notFoundHandler'] = function ($c) {
	return function ($request, $response) use ($c) {
		return $c->view->render($response->withStatus(404), "notfound");
	};
};


/* functions */
require_once "functions.php";

/* Route files */
require_once "routes/main.php";

// Run app
$app->run();

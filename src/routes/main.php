<?php

/* Home page */
$app->get('/', function ($request, $response, $args) {

	// set genres var
	$this->view->set('genres', $this->tmdb->genres);

	list($_searchBy, $_searchQuery) = call_user_func(function ($get){
		if (!isset($_GET['searchby']) || empty($_GET['searchby']))
			return [false, null];
		if ($_GET['searchby']=='name' && isset($_GET['name']))
			return ['name', $get['name']];
		if ($_GET['searchby']=='genre' && isset($_GET['genre']))
			return ['genre', $get['genre']];
	}, $_GET);

	// search
	if ($_searchBy == 'name')
		$this->view->set('movies', $this->tmdb->searchByName($_searchQuery));
	elseif ($_searchBy == 'genre')
		$this->view->set('movies', $this->tmdb->searchByGenre($_searchQuery));
	else
		$this->view->set('movies', $this->tmdb->trending());


	return $this->view->render($response, 'home', $args);
});



/* Movie page */
$app->get('/movie/{id}[/]', function ($request, $response, $args) {
	$this->view->set('movie', $this->tmdb->movie($args['id']));
	return $this->view->render($response, 'movie', $args);
});

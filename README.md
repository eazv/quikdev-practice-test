# Eduardo's QuikDev Practice Test
Evaluation test for full-stack developer

## Setting up

 - The code should preferably be executed in **php 5.6** or **higher**.
 - To develop I used the internal php server with the command `php -S
   localhost: 8080 -t ./web/` in the project directory, but it can also
   run on Apache.
 - Dependencies are managed with composer, to install use
   the command `composer install --dev` in the project directory. In
   order to run the tests use the command `php vendor / bin / phpunit`
   in the project directory

I made this implementation in a linux environment, some commands may vary in other systems


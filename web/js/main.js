
jQuery(function($){

	var checkFormFields = function () {
		if ($('#searchBy select').val()=='name') {
			$('#searchByName').show();
			$('#searchByGenre').hide();
		} else {
			$('#searchByName').hide();
			$('#searchByGenre').show();
		}
	};
	checkFormFields();

	$('#searchBy').on('change', checkFormFields);
});

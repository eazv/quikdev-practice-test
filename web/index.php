<?php
/**
 * index file
 * All http requests must be redirected to this script file in order to be properly routed
 */


// directory separator shortcut
if (!defined('DS'))
	define('DS', DIRECTORY_SEPARATOR);

// APP DIR
if (!defined('__LOCAL__'))
	define('__LOCAL__', dirname(__DIR__).DS);

// define the default temp folder
if (!defined('__TEMP__'))
	define('__TEMP__', __LOCAL__.DS.'var'.DS.'temp'.DS, true);
	//define('__TEMP__', sys_get_temp_dir().DS, true);


//
require_once __LOCAL__.'vendor/autoload.php';

//
require_once __LOCAL__.'src/app.php';
